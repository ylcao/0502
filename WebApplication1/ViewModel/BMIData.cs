﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.ViewModel
{
    public class BMIData
    {
        [Display(Name="weight")]
        [Required(ErrorMessage ="required")]
        [Range(30,150,ErrorMessage ="30-150")]
        public float Weight { get; set; }
        public float Height { get; set; }
        public float BMI { get; set; }
        public String level { get; set; }

    }
}